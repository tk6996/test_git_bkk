package main

import "github.com/gofiber/fiber/v2"

func main() {
	app := fiber.New()

	app.Get("/head", func(c *fiber.Ctx) error {
		return c.SendString("This is Head")
	})
	app.Get("/pop", func(c *fiber.Ctx) error {
		return c.SendString("This is member")
	})
	app.Get("/Aom", func(c *fiber.Ctx) error {

		return c.SendString("Member!")
	})

	app.Get("/Mookmik", func(c *fiber.Ctx) error {
		return c.SendString("Member")
	})

	app.Get("/rak", func(c *fiber.Ctx) error {
		return c.SendString("This is member")
	})

	app.Get("/jon", func(c *fiber.Ctx) error {
		return c.SendString("This is member")
	})

	app.Get("/bay", func(c *fiber.Ctx) error {
		return c.SendString("This is member")
	})
	app.Listen(":3000")
}
